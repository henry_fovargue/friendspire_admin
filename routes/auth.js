var express = require('express');
var router = express.Router();

// user authentication and redirection
router.use(function (req, res, next) {
    if (req.user) {
        res.locals.user = req.user;
        res.locals.session = req.session;
        res.set('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        next();
    } else if (req.xhr) {
        res.send('unauthorised');
    } else {
        res.redirect('/login');
    }
});

module.exports = router;
