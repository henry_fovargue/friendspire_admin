var express = require('express');
var router = express.Router();
const controller = require("../controllers/userController");



/* GET method for list and search Users  */
router.get('/', controller.get_users);

// get user details
router.get('/user_detail/:id', controller.get_user_detail);

router.get('/getcsv', controller.getcsv);
// disable user
router.post('/disable_user', controller.post_disable_user);

//predictive search
// router.post('/search_by',controller.predictive_search);


module.exports = router;

