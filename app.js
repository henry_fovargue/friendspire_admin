/*
 * Debut Infotect
 * Project: Bottle Drive
 * Page:app.js
 * Date: /12/2017
 * Created By: Debut
 */

// require connection file to connect mongo
require('./connection');

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
// require connect-flash
var flash = require('connect-flash');
//for caching file
var staticify = require('staticify')(path.join(__dirname, 'public'));
// require passport module
var passport = require('passport');

// require session module
var session = require('express-session');
// to help secure Express/Connect apps with various HTTP headers
var helmet = require('helmet');
var compression = require('compression');
require('paginate-for-mongoose');
// reuire swig module
var swig = require('swig');
var config = require('./config');

require("./routes/cron");
// require mongoose module
var app = express();
app.use(compression());
app.use(helmet());
// app.use(helmet.noCache());
app.use(helmet.referrerPolicy({ policy: 'same-origin' }));
app.set('env', config.env.name || 'development');

global.serviceurl = config.env.serviceUrl;
global.adminurl = config.env.adminUrl;
global.timeformat = config.timeformat;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.set('trust proxy', 1);

// setting swig template engine
app.engine('html', swig.renderFile);

// uncomment after placing your favicon in /public
// app.use( favicon(path.join( __dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());


// middleware for init the passport module
app.use(session({ name: "friendSpire_session_id", secret: global.secret, resave: false, saveUninitialized: false })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session



/*staticify*/
app.use(staticify.middleware);
app.locals = {
    getVersionedPath: staticify.getVersionedPath
};

/**

 * static assets in your public directory will be cached for 30 days!

 * Visitors won’t have to re-download your CSS, images, or any other static assets in /public.

 */

app.use(function (req, res, next) {
    req.url = req.url.replace(/\/([^\/]+)\.[0-9a-f]+\.(css|js|jpg|png|gif|svg)$/, '/$1.$2');
    next();
});

app.use(express.static(path.join(__dirname, 'public'), { maxAge: '10 days' }));
/* end staticify */

app.use(function (req, res, next) { 
    next();
});

app.use('/email_verification', require('./routes/email_verification'));
app.use('/', require('./routes/index'));


// auth routes for all rutes
 app.all('*', require('./routes/auth'));

// middlewares for routes
app.use('/dashboard', require('./routes/dashboard'));
app.use('/emailTemplate', require('./routes/emailTemplate'));
app.use('/contact_us', require('./routes/contact_us'));
app.use('/faqs', require('./routes/faqs'));
app.use('/otp_verification', require('./routes/otp_verification'));
//app.use('/pages', require('./routes/pages'));
app.use('/users', require('./routes/users'));
app.use('/reviews', require('./routes/reviews'));
app.use('/get_contact_messages', require('./routes/get_contact_messages'));
app.use('/get_notifications', require('./routes/get_notifications'));


// middleware for logout
app.get('/logout', function (req, res) {
    req.logout();
    req.session.destroy();
    res.redirect('/login');
});

// error handlers

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Actually the page you are looking for does not exist.');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    if (err.status === 0 || err.status === undefined) {
        err.status = 500;
    }
    // render the error page
    res.status(err.status || 500);
    res.render('error', { error: err });
});

module.exports = app;
