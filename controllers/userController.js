"use strict";
const Promise = require('bluebird');
const users = require('../models/user.js');
const moment = require('moment');
const mongoose = require('mongoose');
const mails = require('../helper/send_mail.js');
const email = require('../email_template_cms_pages');
const email_templates = require('../models/email_template.js');
const util = require("util");
/** get user listing */
exports.get_users = (req, res, next) => {


    new Promise((resolve, reject) => {
// make global variable options for paginate method parameter
        let options = {
            perPage: 10,
            delta: 4,
            page: 1
        };
        /** ***condition for not deleted users*****/
        let condition = {
            type: {$ne: 1},
            status: {$ne: 2},
            is_deleted: 0
        };
        let sortCondition = {};
        /** ***fetch conditions*****/
        var searchby = req.query.search_by;
        if (searchby) {
            if (searchby.charAt(0) === '+') {
                searchby = searchby.slice(1);
            }
            searchby = searchby.trim();
            let search_array = searchby.split(' ');
            if (search_array.length === 1) {
                var search_by = new RegExp('.*' + searchby + '.*', 'i');
                condition.$or = [{firstName: search_by}, {lastName: search_by}, {email: search_by}];
            } else {
                let firstName = new RegExp(search_array[0], 'i');
                let lastName = new RegExp('.*' + search_array[1] + '.*', 'i');
                condition.$and = [{firstName: firstName}, {lastName: lastName}, {email: search_by}];
            }
        }

        /** add conditons if gender and status is added */

        if (req.query.status && req.query.gender) {
            if (req.query.status != "all") {
                condition.status = req.query.status;
            }
            if (req.query.gender != "all") {
                condition.gender = req.query.gender;
            }
        } else if (req.query.status) {
            if (req.query.status != "all") {
                condition.status = req.query.status;
            }
        } else if (req.query.gender) {
            if (req.query.gender != "all") {
                condition.gender = req.query.gender;
            }
        }



        /** ***pagination check*****/
        if (req.query.page) {
            options.page = req.query.page;
        }
        /** ***skip check*****/
        let skipRecords = options.page - 1;
        let projection = {
            firstName: 1,
            lastName: 1,
            gender: 1,
            email: 1,
            type: 1,
            status: 1,
            created_at: 1,
            is_deleted: 1
        };
        let population = [];
        users.find_with_pagination_projection(
                condition,
                projection,
                population,
                sortCondition,
                options.perPage * skipRecords,
                options.perPage,
                options).then((user_data) => {

            if (user_data.status === 1) {
                let userData = user_data.data;
                // for search query
                if (req.query.page || req.query.search) {
                    res.render('users/search', {response: userData});
                } else // for simple query
                {
                    res.render('users/usersList', {
                        response: userData,
                        message: req.flash(),
                        title: 'Users',
                        delta: 4,
                        active: 'user_page'
                    });
                }
            } else {
                res.redirect('/users');
            }
        });
    }).catch(err => {
        res.redirect('/users');
    });
};
/**get user details */
exports.get_user_detail = (req, res, next) => {
    new Promise((resolve, reject) => {
        let id = req.params.id;
        let aggregation = [
            {$match: {_id: mongoose.Types.ObjectId(id), is_deleted: 0}},
            {
                "$lookup": {
                    from: "users",
                    let: {ref_id: "$_id"},
                    pipeline: [
                        {
                            $match:
                                    {
                                        $expr: {
                                            $in: ["$$ref_id", "$blocked_user"]
                                        },
                                    }
                        },
                        {'$group': {'_id': null, 'ids': {$push: "$_id"}}},
                        {'$project': {_id: 0, "ids": 1}},
                    ],
                    as: "usersBlockMe"
                }
            },
            {$unwind: {path: "$usersBlockMe", preserveNullAndEmptyArrays: true}},
            {$project: {email: 1, password: 1, last_login: 1, is_private: 1, distance_unit: 1, blocked_user: 1, friends: 1, address: 1, is_deleted: 1, is_email_verified: 1, fb_connected: 1, sns_type: 1, lastName: 1, firstName: 1, status: 1, gender: 1, usersBlockMe: {$cond: {if : "$usersBlockMe", then: "$usersBlockMe.ids", else: []}}}},
            {"$project": {
                    firstName: 1,
                    lastName: 1,
                    email: 1,
                    status: 1, // 0-inactive,1-active,2-otp_verification_pending
                    phone: 1,
                    country_code: 1,
                    gender: 1, //1-male,2-female,3-others
                    dob: 1,
                    profile_pic: 1,
                    push_notification: 1,
                    device_token: 1,
                    is_email_verified: 1,
                    is_email_changed: 1,
                    is_deleted: 1, //1-deleted by admin
                    is_address_added: 1,
                    address: 1,
                    longlat: 1,
                    friends: 1,
                    distance_unit: 1, //   1- miles 2-kilometer
                    last_login: 1,
                    is_private: 1,
                    usersBlockMe: 1,
                    "friend_count": {
                        "$size": {
                            "$filter": {
                                "input": "$friends",
                                "cond": {$and:
                                            [
                                                {"$eq": ["$$this.status", 1]},
                                                {"$eq": ["$$this.is_blocked", 0]},
                                                {"$not":[{"$in": ["$$this.user_id", "$usersBlockMe"]}]}
                                            ]
                                }
                            }
                        }
                    }
                }}
        ];
        users.aggregate(aggregation).then((user_data) => {
            if (user_data.status === 1) {
//                console.log(util.inspect(user_data, {depth: null}));
                if (user_data["data"].length > 0) {
                    let userDetailData = user_data.data[0];
//                    console.log(userDetailData)

                    // userDetailData.created_at = moment(userDetailData.created_at).utcOffset(parseInt(req.cookies.time_zone_offset)).format(global.timeformat);
                    if (userDetailData.dob) {
                        userDetailData.dob = moment(userDetailData.dob);
                    }
                    if (userDetailData.last_login) {
                        userDetailData.last_login = (userDetailData.last_login);
                    }
//                    console.log("userDetailData");
//                    console.log(userDetailData);
// res.render('users/userDetail', {message: req.flash(), back_url: req.query.refer, userDetailData: userDetailData, title: 'User Detail', active: 'user_page'});
                    res.send({status: 1, message: req.flash(), back_url: req.query.refer, userDetailData: userDetailData, title: 'User Detail', active: 'user_page'});
                } else {
                    res.send({status: 0});
                }
            } else {
                res.send({status: 0});
            }
        });
    }).catch(err => {
        console.log(err);
        res.render('error', {error: err});
    });
};
/**enable/disable user */
exports.post_disable_user = (req, res, next) => {
    new Promise((resolve, reject) => {
        let id = req.body.userId;
        var status = req.body.status;
        let remarks = req.body.disable_reason;
        users.findOne({_id: id, is_deleted: 0}).then(client_data => {
            if (client_data.status === 1) {
                var user_status;
                if (status == 1) {
                    user_status = 'Activated';
                }

                if (status == 0) {
                    user_status = 'Inactivated';
                }

                let temp_id = email.enable_disable_user;
                email_templates.findOne({_id: temp_id}).then(template => {
                    if (template.status === 1) // && client_data.data.is_email_verified === 1
                    {
                        var content = template.data.content;
                        content = content.replace('@name@', client_data.data.firstName);
                        content = content.replace('@status@', user_status);
                        content = content.replace('@remarks@', remarks);
                        let subject = template.data.subject;
                        subject = subject.replace('@status@', user_status);
                        mails.send(client_data.data.email, subject, content);
                    }
                    users.update({_id: id}, {$set: {status: req.body.status}}).then(() => {
                        req.flash('success', 'User has been ' + user_status + ' successfully');
                        res.send({'status': 1});
                    });
                });
            } else {
                res.send({'status': 0, message: client_data.message});
            }
        });
    }).catch(err => {
        console.log(err);
        res.send({'status': 0, message: err.message});
    });
};

exports.getcsv = () => {
    try {
        let condition = {
            type: {$ne: 1},
            status: {$ne: 2},
        };
        let sortCondition = {};
        /** ***fetch conditions*****/
//        var searchby = req.query.search_by;
//        if (searchby) {
//            if (searchby.charAt(0) === '+') {
//                searchby = searchby.slice(1);
//            }
//            searchby = searchby.trim();
//            let search_array = searchby.split(' ');
//            if (search_array.length === 1) {
//                var search_by = new RegExp('.*' + searchby + '.*', 'i');
//                condition.$or = [{firstName: search_by}, {lastName: search_by}, {email: search_by}];
//            } else {
//                let firstName = new RegExp(search_array[0], 'i');
//                let lastName = new RegExp('.*' + search_array[1] + '.*', 'i');
//                condition.$and = [{firstName: firstName}, {lastName: lastName}, {email: search_by}];
//            }
//        }

        /** add conditons if gender and status is added */

//        if (req.query.status && req.query.gender) {
//            if (req.query.status != "all") {
//                condition.status = parseInt(req.query.status);
//            }
//            if (req.query.gender != "all") {
//                condition.gender = parseInt(req.query.gender);
//            }
//        } else if (req.query.status) {
//            if (req.query.status != "all") {
//                condition.status = parseInt(req.query.status);
//            }
//        } else if (req.query.gender) {
//            if (req.query.gender != "all") {
//                condition.gender = parseInt(req.query.gender);
//            }
//        }
//        console.log("condition to find");
//        console.log(condition);
        var reportData = [];
        let aggregation = [
            {$match: condition},
            {
                "$lookup": {
                    from: "users",
                    let: {ref_id: "$_id"},
                    pipeline: [
                        {
                            $match:
                                    {
                                        $expr: {
                                            $in: ["$$ref_id", "$blocked_user"]
                                        },
                                    }
                        },
                        {'$group': {'_id': null, 'ids': {$push: "$_id"}}},
                        {'$project': {_id: 0, "ids": 1}},
                    ],
                    as: "usersBlockMe"
                }
            },
            {$unwind: {path: "$usersBlockMe", preserveNullAndEmptyArrays: true}},
            {$project: {email: 1, password: 1, last_login: 1, is_private: 1, distance_unit: 1, blocked_user: 1, friends: 1, address: 1, is_deleted: 1, is_email_verified: 1, fb_connected: 1, sns_type: 1, lastName: 1, firstName: 1, status: 1, gender: 1, usersBlockMe: {$cond: {if : "$usersBlockMe", then: "$usersBlockMe.ids", else: []}}}},
            {$unwind: {path: "$friends", preserveNullAndEmptyArrays: true}},
            {$group: {_id: "$_id", user_data: {$first: "$$ROOT"}, friends: {$push: "$friends"}, "following": {"$sum": {"$cond": [{"$and": [{$eq: ["$friends.status", 1]}, {$eq: ["$friends.is_blocked", 0]}, {$not: [{$in: ["$friends.user_id", "$usersBlockMe"]}]}]}, 1, 0]}}}},
            {$replaceRoot: {newRoot: {$mergeObjects: ["$user_data", {"friends": "$friends"}, {"following": "$following"}]}}},
            {
                "$lookup": {
                    from: "users",
                    let: {ref_id: "$_id", blocked_array: "$blocked_user"},
                    pipeline: [
                        {$unwind: "$friends"},
                        {
                            $match:
                                    {
                                        $expr: {
                                            $and: [
                                                {$eq: ["$$ref_id", "$friends.user_id"]},
                                                {$eq: [1, "$friends.status"]},
                                                {$eq: [0, "$friends.is_blocked"]},
                                                {$not: [{$in: ["$_id", "$$blocked_array"]}]}
                                            ]
                                        }
                                    }
                        },
                        {$group: {_id: null, count: {$sum: 1}}},
                    ],
                    as: "followers"
                }
            },
            {
                "$lookup": {
                    from: "recommendations",
                    let: {ref_id: "$_id"},
                    pipeline: [
                        {
                            $match:
                                    {
                                        $expr:
                                                {
                                                    $and:
                                                            [
                                                                {$eq: ["$user_id", "$$ref_id"]},
                                                                {$eq: ["$is_deleted", 0]}
                                                            ]
                                                }
                                    }
                        },
                        {$group: {_id: null, count: {$sum: 1}}}
                    ],
                    as: "recommendations"
                }
            },
            {
                "$lookup": {
                    from: "bookmarks",
                    let: {ref_id: "$_id"},
                    pipeline: [
                        {
                            $match:
                                    {
                                        $expr:
                                                {
                                                    $and:
                                                            [
                                                                {$eq: ["$user_id", "$$ref_id"]},
                                                                {$eq: ["$status", 1]}
                                                            ]
                                                }
                                    }
                        },
                        {$group: {_id: null, count: {$sum: 1}}}
                    ],
                    as: "bookmarks"
                }
            },
            {$unwind: {path: "$followers", preserveNullAndEmptyArrays: true}},
            {$unwind: {path: "$recommendations", preserveNullAndEmptyArrays: true}},
            {$unwind: {path: "$bookmarks", preserveNullAndEmptyArrays: true}},
            {$project: {
                    _id: 0,
                    email: 1,
                    last_login: {"$dateToString": {"format": "%Y-%m-%d %H:%M", "date": "$_id"}},
                    profile_private: {$cond: {if : {$eq: ["$is_private", 1]}, then: "true", else: "false"}},
                    distance_unit: {$cond: {if : {$eq: ["$distance_unit", 1]}, then: "miles", else: "kilometers"}},
                    blocked_user_count: {$size: "$blocked_user"},
                    status: {$cond: {if : {$eq: ["$status", 1]}, then: "active", else: "inactive"}},
                    gender: {$cond: {if : "$gender", then: {$cond: {if : {$eq: ["$gender", 1]}, then: "male", else: {$cond: {if : {$eq: ["$gender", 2]}, then: "female", else: "other"}}}}, else: "NA"}},
                    address: 1,
                    user_deleted: {$cond: {if : {$eq: ["$is_deleted", 1]}, then: "true", else: "false"}},
                    email_verified: {$cond: {if : {$eq: ["$is_email_verified", 1]}, then: "true", else: "false"}},
                    facebook_connected: {$cond: {if : {$eq: ["$fb_connected", 1]}, then: "true", else: "false"}},
                    user_type: {$cond: {if : {$eq: ["$sns_type", 1]}, then: "App user", else: "Facebook user"}},
                    lastName: 1,
                    firstName: 1,
//                    usersBlockMe: 1,
                    following: 1,
                    followers: {$cond: {if : "$followers", then: "$followers.count", else: 0}},
                    total_recommendations: {$cond: {if : "$recommendations", then: "$recommendations.count", else: 0}},
                    total_bookmarks: {$cond: {if : "$bookmarks", then: "$bookmarks.count", else: 0}}
                }
            },
        ];
        users.aggregate(aggregation).then((userData) => {
            if (userData.status == 1) {
                if (userData['data'].length > 0) {
                    var filename = "csv" + Math.floor(Date.now() / 1000).toString() + ".csv";
                    var csvData = ConvertToCSV(userData["data"]);
                    users.findOne({type: 1}, {email: 1}).then(adminemail => {
                        if (adminemail["status"] == 1) {

                            let temp_id = email.csv_export;
                            email_templates.findOne({_id: temp_id}).then(template => {
                                if (template.status === 1) { // && client_data.data.is_email_verified === 1
                                    let today = moment.utc();
                                    var content = template.data.content;
                                    content = content.replace('@startdate@', today.startOf("isoWeek").format("DD-MM-YYYY") + " UTC");
                                    content = content.replace('@enddate@', today.endOf("isoWeek").format("DD-MM-YYYY") + " UTC");
                                    let subject = template.data.subject;
                                    mails.send_with_attachment(adminemail["data"]["email"], subject, content, filename, csvData)
                                }
                            }).catch(error => {
                                throw error;
                            });
                        } else {
                            throw adminemail;
                        }
                    }).catch(error => {
                        throw error;
                    });
                } else {
                    throw {message: "no data found"};
                }
            } else {
                throw userData;
            }
        }).catch(error => {
            throw error;
        })
    } catch (err) {
        console.log(err);
        console.log("error in csv export .csv failed");
    }
};
// convert Json to CSV data
function ConvertToCSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';
    var row = "";
    for (var index in objArray[0]) {
        //Now convert each value to string and comma-separated
        row += index + ',';
    }
    row = row.slice(0, -1);
    //append Label row with line break
    str += row + '\r\n';
    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (var index in array[i]) {
            if (line != '')
                line += ','
            line += '"' + array[i][index] + '"';
        }
        str += line + '\r\n';
    }
    return str;
}





// router.post('/search_by', function (req, res, next) {
//     Sync(function () {
//         try {
//             var name = new RegExp('.*' + req.body.name + '.*', 'i');
//             var user_data = users.find_with_projection.sync(null, {
//                 $or: [
//                     { first_name: name },
//                     { last_name: name },
//                     { email: name }
//                 ]
//             }, { first_name: 1, last_name: 1, email: 1, phone: 1 });
//             if (user_data.status === 1) {
//                 var data = user_data.data;
//                 var id_array = new Array();
//                 for (var i = 0; i < data.length; i++) {
//                     id_array.push((data[i].first_name + ' ' + data[i].last_name).toString());
//                     id_array.push((data[i].email).toString());
//                     id_array.push((data[i].phone).toString());
//                 }

//                 Array.prototype.contains = function (v) {
//                     for (var i = 0; i < this.length; i++) {
//                         if (this[i] === v) { return true; }
//                     }
//                     return false;
//                 };
//                 Array.prototype.unique = function () {
//                     var arr = [];
//                     for (var i = 0; i < this.length; i++) {
//                         if (!arr.contains(this[i])) {
//                             arr.push(this[i]);
//                         }
//                     }
//                     return arr;
//                 };
//                 var duplicates = id_array;
//                 var uniques = duplicates.unique(); // result = [1,3,4,2,8]
//                 res.send(uniques);
//             } else {
//                 res.send([]);
//             }
//         } catch (err) {
//             req.flash('error', err.message);
//             res.redirect('/users');
//         }
//     });
// });


