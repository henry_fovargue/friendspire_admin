$(document).ready(function () {
    $.ajax({
        url: '/dashboard/pie_chart',
        type: "GET",
        data: { search: 1 },
        success: function (result) {
       
            var users_array = [];
            users_array[0] = { label: "Athlete", data: result.Athlete, color: "#0047AB" };
            users_array[1] = { label: "Coach", data: result.Coach, color: "#FF4500" };
            users_array[2] = { label: "supporter", data: result.cheer, color: "#808080" };


            if ($('#chart_1').size() !== 0) {
                $.plot($("#chart_1"), users_array, {
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            label: {
                                show: true,
                                formatter: function (label, series) {
                                    return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">' + label + '<br/>' + Math.round(series.percent) + '%</div>';
                                },
                                background: {
                                    opacity: 3.0
                                }
                            }
                        }
                    },
                    legend: {
                        show: true
                    }
                });
            }
        }
    });

});

