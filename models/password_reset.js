// require mongoose module for mongoose connection
var mongoose = require('mongoose');
var password_reset = mongoose.model('password_reset');

exports.save = (content) => {
    return new Promise((resolve,reject) =>{
        var new_password_reset = new password_reset(content);
        new_password_reset.save(function (error, data) {
            if (!error) {
                resolve( {status: 1, message: 'success', data: data});
            } else {
                if (error.errors) {
                    console.log('error.errors');
                    console.log(error.errors);
                    resolve({status: 2, message: 'Invalid values'});
                } else {
                    resolve({status: 0, message: error.message});
                }
            }
        });
    });
};

// exports.save = (query) => {
//     return new Promise((resolve, reject) => {
//         let new_password_resets = new password_reset(query);
//         new_password_resets.save((error, docs) => {
//             if (error) {
//                 reject(error);
//             } else {
//                 resolve(docs);
//             }
//         });
//     });
// };


exports.insert = (query) => {
    return new Promise((resolve, reject) => {
        // let new_password_resets = new password_reset(query);
        password_reset.insert(query, (error) => {
            if (error) {
                console.log("erooooooooooooo");
                reject(error);
            } else {
                resolve({ success: 1 });
            }
        });
    });
};

// exports.findOne = function (query, callback) {
//     process.nextTick(function () {
//         password_reset.findOne(query, {__v: 0, updated_at: 0, created_at: 0}, function (error, data) {
//             if (!error) {
//                 if (data !== null) {
//                     callback(null, {status: 1, message: 'success', data: data});
//                 } else {
//                     callback(null, {status: 2, message: 'User not found'});
//                 }
//             } else {
//                 callback(null, {status: 0, message: error.message});
//             }
//         });
//     });
// };
exports.findOne = (query) => {
    return new Promise((resolve, reject) => {
        password_reset.findOne(query, { __v: 0, updated_at: 0, created_at: 0 }, (error, data) => {
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
        });

    });
};

// exports.remove = function (query, callback) {
//     process.nextTick(function () {
//         password_reset.remove(query, function (error, data) {
//             if (!error) {
//                 callback(null, {status: 1, message: 'success'});
//             } else {
//                 callback(null, {status: 0, message: error.message});
//             }
//         });
//     });
// };

exports.remove = (query) => {
    return new Promise((resolve, reject) => {
        password_reset.remove(query, (error) => {
            if (error) {
                console.log("error in deletion");
                reject(error);
            } else {
                console.log("deleted");
                resolve({ status: 1, message: 'success' });
            }
        });
    });
};
