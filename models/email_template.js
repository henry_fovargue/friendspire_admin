// require mongoose module for mongoose connection
var mongoose = require('mongoose');
var email_template = mongoose.model('email_template');

/*
 * @params {query} -> query to find data
 * @return success or failure
 */
// exports.findOne = function (query, callback) {
//     process.nextTick(function () {
//         email_template.findOne(query, {__v: 0, updated_at: 0, created_at: 0}, function (error, data) {
//             if (!error) {
//                 if (data !== null) {
//                     data = JSON.parse(JSON.stringify(data));
//                     callback(null, {status: 1, message: 'success', data: data});
//                 } else {
//                     callback(null, {status: 2, message: 'template not found'});
//                 }
//             } else {
//                 callback(null, {status: 0, message: error.message});
//             }
//         });
//     });
// };


/* query for find  user  */
// exports.findOne = (query) => {
//     return new Promise((resolve, reject) => {
//         email_template.findOne(query, {__v: 0, updated_at: 0, created_at: 0},(error, docs) => {
//             if (error) {
//                 reject(error);
//             } else {
//                 resolve(docs);
//             }
//         });
//     });
// };

exports.findOne = (query) => {
    return new Promise((resolve,reject) => {
        email_template.findOne(query, {__v: 0, updated_at: 0, created_at: 0}, (error, data) => {
            if (!error) {
                if (data !== null) {
                    data = JSON.parse(JSON.stringify(data));
                    resolve({status: 1, message: 'success', data: data});
                } else {
                    resolve({status: 2, message: 'template not found'});
                }
            } else {
                resolve({status: 0, message: error.message});
            }
        });
    });
};

exports.find_with_pagination = (query, sort_condition, skip, limit, options) => {
    return new Promise((resolve,reject) => {
        email_template.find(query).sort(sort_condition)
                .skip(skip).limit(limit).paginate(options,(error, data) => {
            if (!error) {
                if (data.length !== 0) {
                    data = JSON.parse(JSON.stringify(data));
                    resolve({status: 1, message: 'success', data: data});
                } else {
                    resolve({status: 2, message: 'email_template not found'});
                }
            } else {
                resolve({status: 0, message: error.message});
            }
        });
    });
};


exports.save = (data) => {
    return new Promise((resolve,reject) =>{
        var new_email_template = new email_template(data);
        new_email_template.save(function (error, data) {
            if (!error) {
                resolve( {status: 1, message: 'success', data: data});
            } else {
                if (error.errors) {
                    console.log('error.errors');
                    console.log(error.errors);
                    resolve({status: 2, message: 'Invalid values'});
                } else {
                    resolve({status: 0, message: error.message});
                }
            }
        });
    });
};


/* 
exports.findOne = (query) => {
    return new Promise ((resolve,reject) =>{
        email_template.findOne(query,  (error, data) => {
             if (!error) {
                 if (data !== null) {
                     data = JSON.parse(JSON.stringify(data));
                     resolve( { status: 1, message: "success", data: data });
                 } else {
                     resolve({ status: 0, message: "email_template not found" });
                 }
             } else {
                 resolve({ status: 0, message: error.message });
             }
         });
     })
           
 }; */

 exports.update = (query, update_data) =>{
    return new Promise((resolve,reject)=> {
        email_template.update(query, update_data, { multi: true }, (error, data) => {
            if (!error) {
                if (data.nModified > 0) {
                    resolve({ status: 1, message: "success" });
                } else {
                    resolve({ status: 0, message: "Users data could not update" });
                }
            } else {
                resolve({ status: 0, message: error.message });
            }
        });
    });
};