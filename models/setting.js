// require mongoose module for mongoose connection
const mongoose = require('mongoose');
const setting = mongoose.model('setting');
const Promise = require('bluebird');
exports.save = (contact_data) => {
    return new Promise((resolve, reject) => {
        // add cms
        let new_setting_req = new setting(contact_data);
        new_setting_req.save((error, data) => {
            if (!error) {
                if (data) {
                    resolve({status: 1, message: 'success', data: data});
                } else {
                    resolve({status: 2, message: 'setting not found'});
                }
            } else {
                resolve({status: 0, message: error.message});
            }
        });
    });
};

exports.find = () => {
    return new Promise((resolve, reject) => {
        setting.findOne({}, {__v: 0, updated_at: 0, created_at: 0}, (error, data) => {
            if (!error) {
                if (data) {
                    resolve({status: 1, message: 'success', data: data});
                } else {
                    resolve({status: 2, message: 'setting not found'});
                }
            } else {
                resolve({status: 0, message: error.message});
            }
        });
    });
};

exports.update = function (query, update_data) {
    return new Promise((resolve, reject) => {
        setting.update(query, update_data, {multi: true}, (error, data) => {
            if (!error) {
                if (data.nModified > 0) {
                    resolve({status: 1, message: 'success'});
                } else {
                    resolve({status: 2, message: 'setting data could not update'});
                }
            } else {
                resolve({status: 0, message: error.message});
            }
        });
    }
    ).catch(err => { // catch errors 
        return Promise.resolve({status: 0, message: err.message});
    });
};
