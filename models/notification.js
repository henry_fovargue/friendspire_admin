

// require mongoose module for mongoose connection
var mongoose = require('mongoose');
var notification = mongoose.model('notification');



exports.findOne = (query, projection) => {
    return new Promise((resolve,reject) =>{
        notification.findOne(query, projection, (error, data) => {
             if (!error) {
                 if (data !== null) {
                     data = JSON.parse(JSON.stringify(data));
                     resolve( { status: 1, message: "success", data: data });
                 } else {
                     resolve({ status: 0, message: "notification not found" });
                 }
             } else {
                 resolve({ status: 0, message: error.message });
             }
         });
     })           
 };
 exports.count = (query) => {
    return new Promise((resolve,reject)=> {
        notification.count(query, (error, data) => {
            if (!error) {
                if (data[0] !== null) {
                    resolve({ status: 1, message: "success", data: data });
                } else {
                    resolve( { status: 0, message: "Match not found" });
                }
            } else {
                resolve({ status: 0, message: error.message });
            }
        });
    });
};

exports.aggregate = (aggregate_query) => {
    return new Promise((resolve,reject)=> {
        notification.aggregate(aggregate_query, (error, data) => {
            if (!error) {
                data = JSON.parse(JSON.stringify(data));
               resolve( { status: 1, message: "success", data: data });

            } else {
                resolve({ status: 0, message: error.message });
            }
        });
    });
};
exports.find_with_update = (query, update_data) =>{
    return new Promise((resolve,reject)=> {
        notification.findOneAndUpdate(query, {$set: update_data},{returnNewDocument: true}, (error, data) => {

            if (!error) {
                if (data) {
                    resolve({ status: 1, message: "success",data: data });
                } else {
                    resolve({ status: 0, message: "notification data could not update" });
                }
            } else {
                resolve({ status: 0, message: error.message });
            }
        });
    });
};

exports.update_many = (query, update_data) =>{
    return new Promise((resolve,reject)=> {
        notification.updateMany(query, update_data, (error, data) => {           
             
            if (!error) {
                if (data.nModified > 0) {
                    resolve({ status: 1, message: "success" });
                } else {
                    resolve({ status: 0, message: "notification data could not update" });
                }
            } else {
                resolve({ status: 0, message: error.message });
            }
        });
    });
};
exports.find_population = (query,projection, populate_query,sort) => {
    return new Promise((resolve,reject) => {      
        notification.find(query,projection).populate(populate_query).sort(sort).exec(function (error, data) {
            if (!error) {
                if (data !== null) {
                    data = JSON.parse(JSON.stringify(data));
                    resolve( {status: 1, message: 'success', data: data});
                } else {
                    resolve( {status: 2, message: 'notification not found'});
                }
            } else {
                resolve({status: 0, message: error.message});
            }
        });
    });
};
// exports.find_with_projection_population_and_pagination = function (query, projection, population, sort_condition, page_no, callback)
// {
//     process.nextTick(function ()
//     {
//         notification.find(query, projection).populate(population).sort(sort_condition).skip((page_no - 1) * 10).limit(10).exec(function (error, data) {
//             if (!error)
//             {
//                 if (data.length !== 0)
//                 {
//                     data = JSON.parse(JSON.stringify(data));
//                     callback(null, {status: 1, message: "success", data: data});
//                 } else
//                 {
//                     callback(null, {status: 1, message: "Notification not found", data: []});
//                 }
//             } else
//             {
//                 callback(null, {status: 0, message: error.message});
//             }
//         });
//     });
// };

// exports.update = function (query, update_data, callback)
// {
//     process.nextTick(function ()
//     {
//         notification.update(query, update_data, {multi: true}, function (error, data)
//         {
//             if (!error)
//             {
//                 if (data.nModified > 0)
//                 {
//                     callback(null, {status: 1, message: "success"});
//                 } else
//                 {
//                     callback(null, {status: 2, message: "Notification data could not update"});
//                 }
//             } else
//             {
//                 callback(null, {status: 0, message: error.message});
//             }

//         });
//     });
// };

// exports.create = function (notification_data, callback)
// {
//     process.nextTick(function ()
//     {
//         if (notification_data.message)
//             notification_data.message = "<font size='4'>" + notification_data.message + "</font>";
        
//         notification.create(notification_data, function (error, data)
//         {
//             if (!error)
//             {
//                 callback(null, {status: 1, message: "success", data: data});
//             } else
//             {
//                 if (error.errors)
//                 {
//                     callback(null, {status: 2, message: "Invalid values"});
//                 } else
//                 {
//                     callback(null, {status: 0, message: error.message});
//                 }
//             }
//         });
//     });
// };

// exports.create_for_admin = function (notification_data, callback)
// {
//     process.nextTick(function ()
//     {
//         notification.create(notification_data, function (error, data)
//         {
//             if (!error)
//             {
//                 callback(null, {status: 1, message: "success", data: data});
//             } else
//             {
//                 if (error.errors)
//                 {
//                     callback(null, {status: 2, message: "Invalid values"});
//                 } else
//                 {
//                     callback(null, {status: 0, message: error.message});
//                 }
//             }
//         });
//     });
// };

// exports.findOne = function (query, callback) {
//     process.nextTick(function () {
//         notification.findOne(query ,function (error, data ) {
//             if (!error) {
//                 if (data) {
//                     callback(null, { status: 0, message: "review is already reported"});
//                 } else {
//                     callback(null, { status: 1, message: "ok"});
//                 }
//             } else {
//                 callback(null, { status: 0, message: error.message });
//             }
//         });
//     });
// };

