var mongoose = require('mongoose');
var recommend = mongoose.model('recommendation');


// exports.findOneAndUpdate = function (query, update_data, callback) {
//     process.nextTick(function () {
//         recommend.findOneAndUpdate(query, update_data, { upsert: true, new: true }, function (error, data) {

//             if (!error) {
//                 if (data !== null) {
//                     callback(null, { status: 1, message: "success", data: data });
//                 } else {
//                     callback(null, { status: 0, message: "Users data could not update" });
//                 }
//             } else {
//                 callback(null, { status: 0, message: error.message });
//             }
//         });
//     });
// };

// exports.findOneWithPopulate = function (query, projection, callback) {
//     process.nextTick(function () {
//         recommend.findOne(query, projection).populate('reference_id').exec( function (error, data) {
//             if (!error) {
//                 if (data) {
//                     callback(null, { status: 1, message: "success", data: data });
//                 } else {
//                     callback(null, { status: 0, message: "Data not found", data: {} });
//                 }
//             } else {
//                 callback(null, { status: 0, message: error.message });
//             }
//         });
//     });
// };

exports.findOne = function (query) {
    return new Promise((resolve,reject) => {
        recommend.findOne(query, function (error, data) {
            if (!error) {
                if (data) {
                    resolve({ status: 1, message: "success", data: data });
                } else {
                    resolve({ status: 0, message: "Data not found", data: {} });
                }
            } else {
                resolve({ status: 0, message: error.message });
            }
        });
    });
};

exports.aggregate = function (aggregate_query) {
    return new Promise((resolve,reject) => {
        recommend.aggregate(aggregate_query, (error, data) => {
            if (!error) {
                data = JSON.parse(JSON.stringify(data));
                resolve({status: 1, message: "success", data: data});
            } else {
                resolve({status: 0, message: error.message});
            }
        });
    });
};

exports.update = (query, update_data) => {
    return new Promise((resolve,reject) => {
        recommend.update(query, {$set: update_data} , function (error, data) {
            console.log(data);
            if (!error) {
                if (data.nModified > 0) {
                    resolve( { status: 1, message: "success", data: data });
                } else {
                    resolve({ status: 0, message: "recommendation data could not update because recommendation already deleted" });
                }
            } else {
                resolve( { status: 0, message: error.message });
            }
        });
    });
};
exports.find_count = (query) => {
    return new Promise((resolve,reject) => { 
        // query.is_deleted = 0;
        recommend.find(query).count().exec((error, data) => {
            if (!error) {
                resolve({status: 1, message: 'success', data: data});
            } else {
                resolve({status: 0, message: error.message});
            }
        }).catch(err => reject(err));
    });
};
// exports.save = function (post_data, callback) {
//     process.nextTick(function () {
//         var new_data = new recommend(post_data);
//         new_data.save(function (error, data) {
//             if (!error) {
//                 data = JSON.parse(JSON.stringify(data));
//                 callback(null, { status: 1, message: "success", data: data });
//             } else {
//                 callback(null, { status: 0, message: error.message });
//             }
//         });
//     });
// };

// exports.update = function (query, update_data, callback) {
//     process.nextTick(function () {
//         recommend.update(query, update_data, function (error, data) {

//             if (!error) {
//                 if (data.nModified > 0) {
//                     callback(null, { status: 1, message: "success", data: data });
//                 } else {
//                     callback(null, { status: 0, message: "User data could not update because user already deleted" });
//                 }
//             } else {
//                 callback(null, { status: 0, message: error.message });
//             }
//         });
//     });
// };

// exports.findWithPagination = function (query, projection, sort_condition, skip, limit, callback) {
//     process.nextTick(function () {
//         recommend.find(query, projection).populate('reference_id').sort(sort_condition).skip(skip).limit(limit).exec(function (error, data) {
//             if (!error) {
//                 if (data) {
//                     callback(null, { status: 1, message: "success", data: data });
//                 } else {
//                     callback(null, { status: 0, message: "Users not found" });
//                 }
//             } else {
//                 callback(null, { status: 0, message: error.message });
//             }
//         });
//     });
// };

// exports.count = function (query, callback) {
//     process.nextTick(function () {
//         recommend.count(query, function (error, data) {
//             if (!error) {
//                 if (data) {
//                     callback(null, { status: 1, message: "success", data: data });
//                 } else {
//                     callback(null, { status: 0, message: "count not found" });
//                 }
//             } else {
//                 callback(null, { status: 0, message: error.message });
//             }
//         });
//     });
// };



// exports.aggregate = function (aggregate_query, callback) {
//     process.nextTick(function () {
//         recommend.aggregate(aggregate_query, function (error, data) {
//             if (!error) {
//                 data = JSON.parse(JSON.stringify(data));
//                 callback(null, { status: 1, message: "success", data: data });

//             } else {
//                 callback(null, { status: 0, message: error.message });
//             }
//         });
//     });
// };


// exports.find = function (query, projection, callback) {
//     process.nextTick(function () {
//         recommend.find(query, projection, function (error, data) {
//             if (!error) {
//                 if (data) {
//                     callback(null, { status: 1, message: "success", data: data });
//                 } else {
//                     callback(null, { status: 0, message: "Data not found" });
//                 }
//             } else {
//                 callback(null, { status: 0, message: error.message });
//             }
//         });
//     });
// };