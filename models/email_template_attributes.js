// require mongoose module for mongoose connection
const mongoose = require('mongoose');
const email_template_attributes = mongoose.model('email_template_attributes');

exports.save = (data) => {
    return new Promise((resolve,reject) =>{
        var new_email_template_attributes = new email_template_attributes(data);
        new_email_template_attributes.save(function (error, data) {
            if (!error) {
                resolve( {status: 1, message: 'success', data: data});
            } else {
                if (error.errors) {
                    console.log('error.errors');
                    console.log(error.errors);
                    resolve({status: 2, message: 'Invalid values'});
                } else {
                    resolve({status: 0, message: error.message});
                }
            }
        });
    });
};


exports.findOne = (query) => {
    return new Promise((resolve,reject) =>{
        email_template_attributes.findOne(query, (error, data) => {
             if (!error) {
                 if (data !== null) {
                     data = JSON.parse(JSON.stringify(data));
                     resolve( { status: 1, message: "success", data: data });
                 } else {
                     resolve({ status: 0, message: "email_template not found" });
                 }
             } else {
                 resolve({ status: 0, message: error.message });
             }
         });
     })
           
 };