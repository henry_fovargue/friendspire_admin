var mongoose = require('mongoose');
var bookmark = mongoose.model('bookmark');


exports.find_count = (query) => {
    return new Promise((resolve,reject) => { 
        // query.is_deleted = 0;
        bookmark.find(query).count().exec((error, data) => {
            if (!error) {
                resolve({status: 1, message: 'success', data: data});
            } else {
                resolve({status: 0, message: error.message});
            }
        }).catch(err => reject(err));
    })
};

